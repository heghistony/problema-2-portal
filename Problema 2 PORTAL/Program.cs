﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4Probleme
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Preluam un numar de la utilizator: ");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            Console.Write($"Aveti de introdus {n} numere:");
            for (int i = 0; i < n; i++)
            {
                v[i] = int.Parse(Console.ReadLine());
            }
            Console.Write("Valorea <<a>> este: ");
            int a = int.Parse(Console.ReadLine());
            int k = 0;
            Console.WriteLine("Valorile din for care respecta conditia sunt: ");
            for (int i = 0; i < n; i++)
            {
                if (v[i] < a)
                {
                    k++;
                }
            }
            Console.WriteLine($"Numarul de valori este de {k} ");
            Console.WriteLine("Valorile din for care respecta conditia sunt: ");
            for (int i = 0; i < n; i++)
            {
                if (v[i] < a)
                {
                    Console.WriteLine($"{v[i]} ");
                }
            }
            Console.ReadKey();
        }
    }
}
